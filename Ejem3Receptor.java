import java.net.*;
import java.io.*;

public class Ejem3Receptor{

	public static void main(String[] args){
		if (args.length!=4)
			System.out.println("este programa requiere 4 argumentos");
		else{
			try{
				InetAddress maquinaEmisora = InetAddress.getByName(args[0]);
				int puertoEmisor = Integer.parseInt(args[1]);
				int miPuerto = Integer.parseInt(args[2]);
				String mensaje = args[3];

				MiSocketDatagrama miSocket = new MiSocketDatagrama(miPuerto);

				miSocket.connect(maquinaEmisora,puertoEmisor);
				for(int i=0;i<10;i++)
					System.out.println(miSocket.recibeMensaje());
				miSocket.enviaMensaje(maquinaEmisora,puertoEmisor,mensaje);
				miSocket.close();
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}
}